
## For Ubuntu 20.04 LTS 

Install `figlet` and `lolcat` packets so pretty hostname print can work:

```
 ____
/ ___|  ___ _ ____   _____ _ __
\___ \ / _ \ '__\ \ / / _ \ '__|
 ___) |  __/ |   \ V /  __/ |
|____/ \___|_|    \_/ \___|_|

 _               _
| |__   ___  ___| |_ _ __   __ _ _ __ ___   ___
| '_ \ / _ \/ __| __| '_ \ / _` | '_ ` _ \ / _ \
| | | | (_) \__ \ |_| | | | (_| | | | | | |  __/
|_| |_|\___/|___/\__|_| |_|\__,_|_| |_| |_|\___|

``` 
To change the SSL certificates go to the `70-ssl-online` and change the line:

`declare -a domains=("example.com" "www.google.com")` to any domains you wish, e.g.:

`declare -a domains=("example.com" "www.youtube.com" "www.reddit.com")`
if you're seeing duplicate motd remove the `show-motd.sh` from /etc/profile.d

<img src="https://i.imgur.com/ZTVRUo7.png" alt="example" width="400"/>
